var url = "https://docs.google.com/spreadsheets/d/1tRLgXVqV52gIWeui6fqM9pim0xs_ZM7AIxoEO2jfze8/edit#gid=0";

function doGet(e) {

    var ss = SpreadsheetApp.openByUrl(url);
    var ws = ss.getSheetByName("Values");
    var list = ws.getRange(1, 1, ws.getRange("A1").getDataRegion().getLastRow(), 1).getValues();

    var htmlListArray = list.map(function (r) {
        return '<option>' + r[0] + '</option>';
    }).join('');

    // Logger.log(htmlListArray);

    var tmp = HtmlService.createTemplateFromFile("page");
    tmp.title = "This is my title";
    tmp.list = htmlListArray;
    return tmp.evaluate();
}

function justClick(value){
    // console.log("helli");
    document.getElementById("demo").innerHTML = "Hello World";

    var ss = SpreadsheetApp.openById("1tRLgXVqV52gIWeui6fqM9pim0xs_ZM7AIxoEO2jfze8");
    var ws = ss.getSheetByName("Data");
    ws.appendRow(["This is just click"+value]);

    Logger.log("Value is: ");
    Logger.log(value);

}

function userClicked(userInfo) {
    // Browser.msgBox("High Five!");
    var ss = SpreadsheetApp.openByUrl(url);
    var ws = ss.getSheetByName("Data");
    ws.appendRow([userInfo.lname, userInfo.fname, userInfo.mname, userInfo.app, new Date(), userInfo.oneName]);

}

function include(filename) {
    return HtmlService.createHtmlOutputFromFile(filename).getContent();
}